# dormitory-manager

#### 介绍
{**以下是 Gitee 平台说明，您可以替换此简介**
Gitee 是 OSCHINA 推出的基于 Git 的代码托管平台（同时支持 SVN）。专为开发者提供稳定、高效、安全的云端软件开发协作平台
无论是个人、团队、或是企业，都能够用 Gitee 实现代码托管、项目管理、协作开发。企业项目请看 [https://gitee.com/enterprises](https://gitee.com/enterprises)}

#### 软件架构
软件架构说明
后端：springboot+springmvc+mybatis
数据库 mysql 


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  学生用户
![输入图片说明](13ea820dc59ff790e7203914b9ea778.png)
![输入图片说明](662c5f338e04237a8335f6232ec31df.png)
2.  宿舍管理员
![输入图片说明](1d9cc9eeb46fe6d223e6fc755e11300.png)
![输入图片说明](72814fd09a8173ec670c6b5919b4800.png)
3.  系统管理员
![输入图片说明](0489578652bec0b50828831038b9a47.png)
![输入图片说明](6b469973953740f440a1ebd0d2fbee1.png)
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
5. 如有任何疑惑的可+++微信。
![输入图片说明](9_1648966917.jpg)

#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
